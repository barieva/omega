package ru.omega.parser.service;


import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.web.multipart.MultipartFile;
import ru.omega.parser.dao.FeedSaverRepository;
import ru.omega.parser.domain.feed.Feed;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(XmlSaverService.class)
public class XmlSaverServiceTest {
    @Mock
    FeedSaverRepository feedSaverRepository;
    @InjectMocks
    XmlSaverService xmlSaverService;

    @Test
    public void save() throws Exception {
        MultipartFile file = getMockMultipartFile();
        Feed feed = new Feed();
        mockXmlMapper(file, feed);
        when(feedSaverRepository.save(feed)).thenReturn(new Feed(1));
        assertEquals(1, xmlSaverService.save(file));
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveWithWrongMultipartFile() throws Exception {
        MultipartFile file = getMockMultipartFile();
        xmlSaverService.save(file);
    }

    private void mockXmlMapper(MultipartFile file, Feed feed) throws Exception {
        XmlMapper xmlMapper = mock(XmlMapper.class);
        when(xmlMapper.readValue(file.getBytes(), Feed.class)).thenReturn(feed);
        PowerMockito.whenNew(XmlMapper.class).withNoArguments().thenReturn(xmlMapper);
    }

    private MultipartFile getMockMultipartFile() throws IOException {
        MultipartFile file = mock(MultipartFile.class);
        byte[] feedBytes = {1};
        when(file.getBytes()).thenReturn(feedBytes);
        return file;
    }
}