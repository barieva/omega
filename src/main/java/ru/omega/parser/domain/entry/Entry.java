package ru.omega.parser.domain.entry;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.omega.parser.domain.content.Content;
import ru.omega.parser.domain.entry.tags.Category;
import ru.omega.parser.domain.entry.tags.Title;
import ru.omega.parser.domain.feed.Feed;
import ru.omega.parser.domain.link.Link;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "entry")
public class Entry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int entry_id;
    @JsonAlias("id")
    private String id;
    @JsonAlias("category")
    @Embedded
    private Category category;
    @JsonAlias("title")
    @Embedded
    private Title title;
    @JsonAlias("updated")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime updated;
    @JsonAlias("author")
    private String author;
    @JsonAlias("summary")
    private String summary;
    @JsonAlias("link")
    @JacksonXmlElementWrapper(useWrapping = false)
    @OneToMany(mappedBy = "entry", cascade = ALL, orphanRemoval = true)
    private List<Link> links = new ArrayList<>();
    @JsonAlias("content")
    @OneToOne(cascade = CascadeType.ALL)
    private Content content;
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "feed_id")
    private Feed feed;
}