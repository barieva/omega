package ru.omega.parser.domain.entry.tags;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Embeddable
public class Category {
    @JsonAlias("term")
    @JacksonXmlProperty(isAttribute = true)
    @Column(name = "category_term")
    private String term;
    @JsonAlias("scheme")
    @JacksonXmlProperty(isAttribute = true)
    @Column(name = "category_scheme")
    private String scheme;
}