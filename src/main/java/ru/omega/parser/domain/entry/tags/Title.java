package ru.omega.parser.domain.entry.tags;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Embeddable
public class Title {
    @JsonAlias("type")
    @JacksonXmlProperty(isAttribute = true)
    @Column(name = "title_type")
    private String type;
}
