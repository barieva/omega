package ru.omega.parser.domain.element;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.omega.parser.domain.contact.information.ContactInformation;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.FetchType.LAZY;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "element")
public class Element {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @JsonAlias("type")
    @JacksonXmlProperty(isAttribute = true)
    @Column(name = "type")
    private String mType;
    @JsonAlias("Ref_Key")
    @Column(name = "Ref_Key")
    private String ref_key;
    @JsonAlias("LineNumber")
    @Column(name = "LineNumber")
    private String lineNumber;
    @JsonProperty("Тип")
    @Column(name = "Тип")
    private String dType;
    @JsonAlias("Вид_Key")
    @Column(name = "Вид_Key")
    private String view_key;
    @JsonAlias("Представление")
    @Column(name = "Представление")
    private String view;
    @JsonAlias("ЗначенияПолей")
    @Column(name = "ЗначенияПолей")
    private String fieldsValues;
    @JsonAlias("Страна")
    @Column(name = "Страна")
    private String country;
    @JsonAlias("Регион")
    @Column(name = "Регион")
    private String region;
    @JsonAlias("Город")
    @Column(name = "Город")
    private String city;
    @JsonProperty("АдресЭП")
    @Column(name = "АдресЭП")
    private String EpAddress;
    @JsonAlias("ДоменноеИмяСервера")
    @Column(name = "ДоменноеИмяСервера")
    private String domainNameServer;
    @JsonAlias("НомерТелефона")
    @Column(name = "НомерТелефона")
    private String phoneNumber;
    @JsonAlias("НомерТелефонаБезКодов")
    @Column(name = "НомерТелефонаБезКодов")
    private String phoneNumberWithoutCodes;
    @JsonAlias("ВидДляСписка_Key")
    @Column(name = "ВидДляСписка_Key")
    private String viewForSheet_key;
    @JsonAlias("ДействуетС")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "ДействуетС")
    private LocalDateTime validAfter;
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "contact_information_id")
    private ContactInformation contactInformation;
}