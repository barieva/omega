package ru.omega.parser.domain.feed;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;
import ru.omega.parser.domain.entry.Entry;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@JacksonXmlRootElement
@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "feed")
@EqualsAndHashCode(of = "id")
public class Feed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public Feed(int id) {
        this.id = id;
    }

    @JsonAlias("base")
    @JacksonXmlProperty(isAttribute = true)
    private String base;
    @JsonAlias("entry")
    @JacksonXmlElementWrapper(useWrapping = false)
    @OneToMany(mappedBy = "feed", cascade = ALL, orphanRemoval = true)
    private List<Entry> entry = new ArrayList<>();
}