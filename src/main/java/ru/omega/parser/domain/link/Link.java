package ru.omega.parser.domain.link;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.omega.parser.domain.entry.Entry;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "link")
public class Link {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @JsonAlias("rel")
    @JacksonXmlProperty(isAttribute = true)
    private String rel;
    @JsonAlias("href")
    @JacksonXmlProperty(isAttribute = true)
    private String href;
    @JsonAlias("type")
    @JacksonXmlProperty(isAttribute = true)
    private String type;
    @JsonAlias("title")
    @JacksonXmlProperty(isAttribute = true)
    private String title;
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "entry_id")
    private Entry entry;
}