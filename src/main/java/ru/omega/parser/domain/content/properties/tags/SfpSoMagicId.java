package ru.omega.parser.domain.content.properties.tags;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter
@NoArgsConstructor
@Embeddable
public class SfpSoMagicId {
    @JacksonXmlProperty(isAttribute = true, localName = "null")
    @Column(name = "properties_сфпCoMagicID_null_column")
    private boolean nullValue;
    @JacksonXmlText
    @Column(name = "properties_сфпCoMagicID")
    private String value;
}
