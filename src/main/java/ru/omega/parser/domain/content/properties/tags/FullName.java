package ru.omega.parser.domain.content.properties.tags;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter
@NoArgsConstructor
@Embeddable
public class FullName {
    @JacksonXmlProperty(isAttribute = true, localName = "null")
    @Column(name = "properties_НаименованиеПолное_null_column")
    private boolean nullValue;
    @JacksonXmlText
    @Column(name = "properties_НаименованиеПолное")
    private String value;
}
