package ru.omega.parser.domain.content.properties;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.omega.parser.domain.contact.information.ContactInformation;
import ru.omega.parser.domain.content.properties.tags.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.OneToOne;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Embeddable
public class Properties {
    @JsonAlias("Ref_Key")
    @Column(name = "properties_Ref_Key")
    private String ref_key;
    @JsonAlias("DataVersion")
    @Column(name = "properties_DataVersion")
    private String dataVersion;
    @JsonAlias("DeletionMark")
    @Column(name = "properties_DeletionMark")
    private String deletionMark;
    @JsonAlias("Parent_Key")
    @Column(name = "properties_Parent_Key")
    private String parent_key;
    @JsonAlias("IsFolder")
    @Column(name = "properties_IsFolder")
    private boolean folder;
    @JsonAlias("Code")
    @Column(name = "properties_Code")
    private String code;
    @JsonAlias("Description")
    @Column(name = "properties_Description")
    private String description;
    @JsonAlias("ГруппаДоступа_Key")
    @Column(name = "properties_ГруппаДоступа_Key")
    private String groupAccess_key;
    @JsonAlias("ИНН")
    @Embedded
    private Inn inn;
    @JsonAlias("КодПоОКПО")
    @Embedded
    private CodePoOkpo codePoOkpo;
    @JsonAlias("Комментарий")
    @Column(name = "properties_Комментарий")
    private String comment;
    @JsonAlias("КПП")
    @Embedded
    private Kpp kpp;
    @JsonAlias("НаименованиеПолное")
    @Embedded
    private FullName fullName;
    @JsonAlias("ОсновнойБанковскийСчет_Key")
    @Embedded
    private MainBankAccount_key mainBankAccount_key;
    @JsonAlias("Ответственный_Key")
    @Embedded
    private Responsible_key responsible_key;
    @JsonAlias("РегистрационныйНомер")
    @Embedded
    private RegistrationNumber registrationNumber;
    @JsonAlias("ФизЛицо_Key")
    @Embedded
    private Individual_key individual_key;
    @JsonAlias("ЮрФизЛицо")
    @Embedded
    private EntityIndividual entityIndividual;
    @JsonAlias("сфпCoMagicID")
    @Embedded
    private SfpSoMagicId sfpSoMagicId;
    @JsonAlias("ДополнительныеРеквизиты")
    @Embedded
    private AdditionalDetails additionalDetails;
    @JsonAlias("КонтактнаяИнформация")
    @OneToOne(mappedBy = "content")
    private ContactInformation contactInformation;
    @JsonAlias("Predefined")
    @Column(name = "properties_Predefined")
    private String predefined;
    @JsonAlias("PredefinedDataName")
    @Column(name = "properties_PredefinedDataName")
    private String predefinedDataName;
}