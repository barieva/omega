package ru.omega.parser.domain.content.properties.tags;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Embeddable
public class AdditionalDetails {
    @JsonAlias("type")
    @JacksonXmlProperty(isAttribute = true)
    @Column(name = "properties_Дополнительные_Реквизиты_type")
    private String type;
}