package ru.omega.parser.domain.content;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.omega.parser.domain.content.properties.Properties;

import javax.persistence.*;


@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "content")
public class Content {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @JsonAlias("type")
    @JacksonXmlProperty(isAttribute = true)
    private String type;
    @JsonAlias("properties")
    @Embedded
    private Properties properties;
}