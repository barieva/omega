package ru.omega.parser.domain.contact.information;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.omega.parser.domain.content.Content;
import ru.omega.parser.domain.element.Element;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "contactInformation")
public class ContactInformation {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;
    @JsonAlias("type")
    @JacksonXmlProperty(isAttribute = true)
    private String type;
    @JsonAlias("element")
    @JacksonXmlElementWrapper(useWrapping = false)
    @OneToMany(mappedBy = "contactInformation", cascade = ALL, orphanRemoval = true)
    private List<Element> element = new ArrayList<>();
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "content_id")
    private Content content;
}