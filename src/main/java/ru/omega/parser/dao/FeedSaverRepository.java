package ru.omega.parser.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.omega.parser.domain.feed.Feed;

@Repository
public interface FeedSaverRepository extends CrudRepository<Feed, Integer> {
}