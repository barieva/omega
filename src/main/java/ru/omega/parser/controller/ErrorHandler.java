package ru.omega.parser.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandler {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> sendException(Exception exception) {
        return new ResponseEntity<>(getJsonMessage(exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    private String getJsonMessage(String text) {
        return "{message: " + text + " }";
    }
}