package ru.omega.parser.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import ru.omega.parser.service.XmlSaverService;

@Controller
public class XmlParserController {
    private XmlSaverService xmlSaverService;

    @Autowired
    public void setXmlSaverService(XmlSaverService xmlSaverService) {
        this.xmlSaverService = xmlSaverService;
    }

    @PostMapping(value = "/save", consumes = "multipart/form-data")
    @ResponseBody
    public int saveXmlFile(@RequestParam("file") MultipartFile file) {
        return xmlSaverService.save(file);
    }

    @GetMapping("/")
    public String forwardToLoadXmlPage() {
        return "forward:static/loadXml.html";
    }
}