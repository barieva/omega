package ru.omega.parser.service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.omega.parser.dao.FeedSaverRepository;
import ru.omega.parser.domain.feed.Feed;

import java.io.IOException;

@Service
@EnableTransactionManagement
public class XmlSaverService {
    private FeedSaverRepository feedSaverRepository;

    @Autowired
    public void setFeedSaverRepository(FeedSaverRepository feedSaverRepository) {
        this.feedSaverRepository = feedSaverRepository;
    }

    @Transactional
    public int save(MultipartFile file) {
        XmlMapper mapper = new XmlMapper();
        mapper.registerModule(new JavaTimeModule());
        try {
            Feed feed = mapper.readValue(file.getBytes(), Feed.class);
            return feedSaverRepository.save(feed).getId();
        } catch (IOException e) {
            throw new IllegalArgumentException("Xml file is not valid");
        }
    }
}